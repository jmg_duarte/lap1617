// Copiar este código para o início de cada programa.

var JSRoot = {
    SUPER: function (method) {
        return method.apply(this,
            Array.prototype.slice.apply(arguments).slice(1));
    },
    INIT: function () {
        throw "*** MISSING INITIALIZER ***";
    }
};

function NEW(clazz) { // Create an object and applies INIT(...) to it
    function F() {}
    F.prototype = clazz;
    var obj = new F();
    obj.INIT.apply(obj, Array.prototype.slice.apply(arguments).slice(1));
    return obj;
};

function EXTENDS(clazz, added) { // Creates a subclass of a given class
    function F() {}
    F.prototype = clazz;
    var subclazz = new F();
    for (prop in added)
        subclazz[prop] = added[prop];
    return subclazz;
};

// "print" é opcional, mas introduz compatibilidade com o Node.js
var print = typeof (console) !== 'undefined' ? console.log : print;

var Point1 = EXTENDS(JSRoot, {
    x: 0,
    INIT: function (x) {
        this.x = x;
    },
    equals: function (that) {
        return this.x == that.x;
    },
    shift: function (deltax) {
        this.x += deltax;
    },
    show: function () {
        print("(" + this.x + ")");
    }
});

var Point2 = EXTENDS(Point1, {
    y: 0,
    INIT: function (x, y) {
        this.SUPER(Point1.INIT, x);
        this.y = y;
    },
    equals: function (that) {
        return this.SUPER(Point1.equals, that) &&
            this.y == that.y;
    },
    shift: function (deltax, deltay) {
        this.SUPER(Point1.shift, deltax);
        this.y += deltay;
    },
    show: function () {
        print("(" + this.x + ", " + this.y + ")");
    }
});

var Point3 = EXTENDS(Point2, {
    z: 0,
    INIT: function (x, y, z) {
        this.SUPER(Point2.INIT, x, y);
        this.z = z;
    },
    equals: function (that) {
        return this.SUPER(Point2.equals, that) &&
            this.z == that.z;
    },
    shift: function (deltax, deltay, deltaz) {
        this.SUPER(Point2.shift, deltax, deltay)
        this.z += deltaz;
    },
    show: function () {
        print("(" + this.x + ", " + this.y + ", " + this.z + ")");
    }
});

var tests = {
    testInheritance: function () {
        var a = NEW(Point3, 1, 2, 3);
        var b = NEW(Point3, 6, 7, 8);
        a.show();
        b.show();
        a.shift(1, 1, 1);
        a.show();
        b.show();
        b.shift(1, 1, 1);
        a.show();
        b.show();
    },
    testMixedTypes: function () { // a parte inválida das operações não tem efeito.
        var a = NEW(Point1, 1);
        var b = NEW(Point2, 6, 7);
        a.show();
        b.show();
        a.shift(1, 1, 1);
        a.show();
        b.show();
        b.shift(1, 1, 1);
        a.show();
        b.show();
    },
    run: function () {
        for (fun in this)
            if (fun != 'run') {
                print(fun);
                this[fun]();
            }
    }
};

var Succ = EXTENDS(JSRoot, {
    a0: 0,
    currElem: 0,
    INIT: function (a0) {
        this.a0 = a0;
        this.first();
    },
    first: function () {
        this.currElem = this.a0;
        return this.curr();
    },
    curr: function () {
        return this.currElem;
    },
    next: function () {
        throw "***MISSING METHOD***";
    },
    at: function (i) {
        this.first();
        for (var j = 0; j < i; i++) {
            this.next();
        }
        return this.curr();
    },
    print: function (n) {
        this.first();
        for (var j = 0; j < n; j++) {
            print(this.next());
        }
    }
})

var Const = EXTENDS(Succ, {
    next: function () {
        return this.curr;
    }
})

var Arith = EXTENDS(Succ, {
    inc: 0,
    INIT: function (a0, inc) {
        this.SUPER(Succ.INIT, a0);
        this.inc = inc;
    },
    next: function () {
        var value = this.curr();
        this.currElem += this.inc;
        return value;
    }
})

var testsSucc = {
    testBase: function () {
        var c = NEW(Succ, 20);
        print(c.first());
        print(c.curr());
        print(c.at(0));
    },
    testConstant: function () {
        var c = NEW(Const, 21);
        print(c.first());
        print(c.curr());
        print(c.at(10));
        c.print(3);
    },
    testArith: function () {
        var c = NEW(Const, 22, 3);
        print(c.first());
        print(c.curr());
        print(c.at(5));
        c.print(3);
    },
    run: function () {
        this.testBase();
        this.testConstant();
        this.testArith();
    }
}

testsSucc.run();
