(* Writes in a list the successor of the given number *)
let rec succAll : int list -> int list = function
	| [] -> []
	| h :: t -> (h +1) :: (succAll t)
;;

(* Checks if a number belongs to a list *)
let belongs el el_list =
	let rec aux = function
		| [] -> false
		| [x] -> if el = x then true else false
		| h :: t -> if el = h then true else aux t
	in aux el_list
;;

let rec alt_belongs el el_list =
	match el_list with
	| [] -> false
	| [x] -> if el = x then true else false
	| h :: t -> if el = h then true else alt_belongs el t
;;

let rec alt2_belongs el el_list =
	match el_list with
	| [] -> false
	| h:: t -> belongs el t || el = h
;;

(* Returns the union of the given lists *)
let union l r =
	let rec aux acc = function
		| [] -> acc
		| h :: t -> if belongs h acc then aux acc t else aux (h :: acc) t
	in aux l r
;;

let rec alt_union l1 l2 =
	match l1 with
	| [] -> l2
	| h :: t -> if belongs h l2 then alt_union t l2 else alt_union t (h:: l2)
;;

(* Returns the intersection between the given lists *)
let rec inter l1 l2 =
	match l1 with
	| [] -> []
	| x :: xs -> if belongs x l2 then x :: (inter xs l2) else inter xs l2
;;

(**)
let rec diff l1 l2 =
	match l1 with
	| [] -> []
	| x :: xs -> if belongs x l2 then diff xs l2 else (x :: diff xs l2)
;;

(* Returns the power set of the given list*)
let rec appendToAll x l =
	match l with
	| [] -> []
	| y :: ys -> (x :: y) :: appendToAll x ys
;;

let rec power l =
	match l with
	| [] -> [[]]
	| x :: xs -> power xs @ appendToAll x (power xs)
;;

(**)
let pack l =
let rec aux l numOccur=
	match l with
	| [] -> []
	| smaller -> smaller
	| x :: (y :: _ as t) -> if x = y then aux t (numOccur+1) else (x, numOccur) :: aux t 0
in aux l 0
;;

(* Writes the natural numbers preceding to the given natural *)
let nat num =
	let rec aux acc num =
		if num <= 0 then []
		else num - 1 :: aux acc (num - 1)
	in aux [] num
;;