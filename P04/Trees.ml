type 'a tree = Nil | Node of 'a * 'a tree * 'a tree;;

let rec make l = 
	match l with 
	| [] -> Nil
	| [x] -> Node(x, Nil, Nil)
	| x::xs -> Node(x, Nil, make xs)
;;

let rec max t = 
	match t with
	| Nil -> 0
	| Node (x, y, z) -> Pervasives.max x (Pervasives.max (max y) (max z))
;;

let rec treeToList l =
v
;;

let rec height t = 
	match t with
	| Nil -> 0
	| Node (_, l, r) -> 1 + max (height l) (height r)
;;	

let rec balanced t =
	match t with
	| Nil -> true
	| Node (_, l, r) -> 
		balanced l && balanced r && abs(height l - height r) <= 1
;;

let rec subtrees t =
	match t with
	| Nil -> [Nil]
	| Node (e, st, Nil) | Node (e, Nil, st)-> [t] @ (subtrees st)
	| Node (e, l, r) -> [t] @ (subtrees l) @ (subtrees r)
;;

let rec spring value t =
	match t with
	| Nil -> Node (value, Nil, Nil)
	| Node (v, l, r) -> Node(v, spring value l, spring value r)
;;