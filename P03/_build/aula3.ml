(************************************************************
21 - Considere o tipo árvore binária em OCaml:
*)
type 'a tree = Nil | Node of 'a * 'a tree * 'a tree;;

(* Escreva as seguintes três funções sobre árvores: howMany : 'a -> 'a *)
(* tree -> int Conta número de ocorrências do valor na árvore           *)

let rec howMany x t =
	match t with
	| Nil -> 0
	| Node (v, l, r) ->
			if v = x then
				howMany x l + howMany x r + 1
			else
				howMany x l + howMany x r
;;

let t1 = Nil;;

let t2 = Node(0, t1, t1);;

let t3 = Node(1, t1, t1);;

let t4 = Node(0, t3, t3);;

let t5 = Node(1, t3, t2);;

assert (howMany 1 t1 = 0);;
assert (howMany 1 t2 = 0);;
assert (howMany 1 t3 = 1);;
assert (howMany 1 t4 = 2);;
assert (howMany 1 t5 = 2);;

(* eqPairs : ('a * 'a) tree -> int Conta número de pares com as duas      *)
(* componentes iguais                                                      *)

let rec eqPairs t =
	match t with
	| Nil -> 0
	| Node ((v1, v2), l, r) ->
			if v1 = v2 then
				eqPairs l + eqPairs r + 1
			else
				eqPairs l + eqPairs r
;;

let p1 = (1,2);;
let p2 = (3,3);;

let pt2 = Node(p1, Nil, Nil);;
let pt3 = Node(p2, Nil, Nil);;

assert (eqPairs Nil = 0);;

assert (eqPairs pt2 = 0);;

assert (eqPairs pt3 = 1);;

(* treeToList : 'a tree -> 'a list Converte árvore em lista, por uma      *)
(* ordem qualquer                                                          *)

let rec treeToList t = 
	match t with
	| Nil -> []
	| Node(v, l, r) -> v :: treeToList l @ treeToList r
;;

(************************************************************
22 - Programe uma função que determine se uma árvore binária está ou não equilibrada.
Uma árvore binária diz - se equilibrada se, para cada nó, a diferença de profundidades
das suas subárvores não superar a unidade. (Copie a função a função auxiliar height da teórica 4.)

balanced: 'a tree -> bool
*)

let rec height t =
	match t with
	| Nil -> 0
	| Node(_,l,r) -> 1 + max (height l) (height r)
;;

let rec balanced t = 
	match t with
	| Nil -> true 
	| Node(_,l,r) -> 
		balanced l && 
		balanced r && 
		abs(height l - height r) <= 1
;;

assert (balanced Nil = true);;
assert (balanced (Node(3, Node(5, Nil, Nil), Node(6, Nil, Nil))) = true);;
assert (balanced (Node(1, Nil, Node(2, Nil, Node(3, Nil, Nil)))) = false);;

(************************************************************
23 - Programe uma função que produza a lista de todas as
subárvores distintas que ocorrem na árvore argumento.

subtrees: 'a tree -> 'a tree list
*)

(*
let subtrees t = 
	match t with
	| Nil -> 
;;
assert (subtrees (Node(5, Nil, Node(6, Nil, Nil))) = [Node(5, Nil, Node(6, Nil, Nil)); Node(6, Nil, Nil); Nil]);;
assert (subtrees Nil = [Nil]);;
*)

(************************************************************
24 - Primavera. Programe uma função que faça crescer novas folhas
em todos os pontos duma árvore onde esteja Nil. As novas
folhas são todas iguais entre si.

spring: 'a -> 'a tree -> 'a tree
*)

let rec spring x t = 
	match t with
	| Nil -> Node(x, Nil, Nil)
	| Node(y,l,r) -> Node(y, spring x l, spring x r)
;;

(************************************************************
25 - Outono. Programe uma função que elimine todas as folhas
existentes duma árvore. Os nós interiores que ficam a descoberto
tornam - se folhas, claro, as quais já não são para eliminar.

fall: 'a tree -> 'a tree
*)

let rec fall t = 
	match t with
	| Nil -> Nil
	| Node (_, Nil, Nil) -> Nil
	| Node (x, Nil, r) -> Node(x, Nil, fall r)
	| Node (x, l, Nil) -> Node(x, fall l, Nil)
	| Node (x, l, r) -> Node(x, fall l, fall r)
;;

(************************************************************
26 - Repita os exercícios 23, 24 e 25, mas agora para árvores n -árias.
O tipo árvore n -ária em OCaml define - se assim:

Nota: Uma folha duma árvore binária tem dois Nil por baixo,
mas uma folha duma árvore n -ária costuma ter apenas uma lista
vazia de filhos, portanto sem qualquer Nil.
*)

type 'a ntree = NNil | NNode of 'a * 'a ntree list

(* ntreeToList : 'a ntree -> 'a list *)

let rec ntreeToList t = match t with
	| NNil -> []
	| NNode(v, l) -> v :: ntreeToList l 
;;

(* nsubtrees : 'a ntree -> 'a ntree list *)

let nsubtrees t = assert false;;

(* nspring: 'a -> 'a ntree -> 'a ntree *)

let nspring x t = assert false;;

(* nfall: 'a ntree -> 'a ntree *)

let nfall t = assert false;;

(************************************************************
27 - Defina um tipo soma exp para representar expressões algébricas
com uma variável real "x". Nas expressões devem poder ocorrer os
operadores binários "+", "-", "*", "/", o operador unário "-",
a variável "x" e ainda literais reais. Exemplos de expressões:
2 * x2 +5
2 * x7 +5 * (x -5)2 -3

Depois de definido o tipo usando um tipo soma como este *)

type exp =
		Add of exp * exp
	| Sub of exp * exp
	| Mult of exp * exp
	| Div of exp * exp
	| Power of exp * int
	| Sim of exp
	| Const of float
	| Var
;;

(* escreva as seguintes funções sobre expressões algébricas: a) eval:  *)
(* float -> exp -> float Avalia a expressão no ponto dado                 *)

let eval x e = assert false;;

(* b) deriv: exp -> exp Determina a derivada em ordem a "x". Não          *)
(* simplifique o resultado.                                                *)

let deriv e = assert false;;

(* Usando esta representação, a expressão 2*x2+5 escreve-se: . *)

let e = Add(Mult(Const 2.0, Power(Var,2)), Const 5.0);;
