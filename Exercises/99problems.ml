(* Last element of a list *)
let rec last = function
	| [] -> None
	| [x] -> Some x
	| _ :: t -> last t
;;

(* Last and penultimate element of a list *)
let rec last_two = function
	| [] | [_] -> None
	| [x; y] -> Some (x, y)
	| _ :: t -> last_two t
;;

(* k'th element of a list *)
let rec at k = function
	| [] -> None
	| h :: t -> if k = 1 then Some h else at (k-1) t
;;

(* Length of a list *)
let length list =
	let rec aux n = function
		| [] -> n
		| _ :: t -> aux (n+1) t
	in aux 0 list
;;

(* Reverse a list *)
let rev list =
	let rec aux acc = function
		| [] -> acc
		| h :: t -> aux (h :: acc) t
	in aux [] list
;;

(* Is a palindrome *)
let is_palindrome list =
	list = List.rev list
;;

(* Flatten a nested list structure *)
type 'a node =
	| One of 'a
	| Many of 'a node list
;;

let flatten list =
	let rec aux acc = function
		| [] -> acc
		| One x :: t -> aux (x :: acc) t
		| Many l :: t -> aux (aux acc l) t
	in List.rev (aux [] list)
;;

(* Eliminate consecutive duplicates *)
let rec compress = function
	| a :: (b :: _ as t) -> if a = b then compress t else a :: compress t
	| smaller -> smaller
;;

(* Pack consecutive duplicates into sublists *)
let pack list =
	let rec current acc = function
		| [] -> []
		| [x] -> (x :: current) :: acc
		| a :: (b :: _ as t) ->
			if a = b then aux (a :: current) acc t
			else aux [] ((a :: current) :: acc) t
	in List.rev (aux [] [] list)
;;

(* Run-length encoding *)
let encoding list =
	let rec current acc = function
		| [] -> []