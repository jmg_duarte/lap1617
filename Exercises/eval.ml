type exp =
      Add of exp*exp
    | Sub of exp*exp
    | Mult of exp*exp
    | Div of exp*exp
    | Power of exp*int
    | Sim of exp
    | Const of float
    | Var
;;

let rec eval x_val expression =
	match expression with
	| Const v -> v 
	| Var -> x_val
	| Power (base, e) -> (eval x_val base) ** (float_of_int e)
	| Mult (l, r) -> (eval x_val l) *. (eval x_val r)
	| Div (l, r) -> (eval x_val l) /. (eval x_val r)
	| Add (l, r) -> (eval x_val l) +. (eval x_val r)
	| Sub (l, r) -> (eval x_val l) -. (eval x_val r)
	| Sim s -> eval (-1.*.x_val) s
;;