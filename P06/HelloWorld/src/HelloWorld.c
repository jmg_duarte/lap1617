/*
 ============================================================================
 Name        : HelloWorld.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int x;

int f(int a){
	x = a + 1;
	return x;
}

int g(int a){
	x = a - 1;
	return x;
}

int order(int a, int b){
	return a + b;
}

void exercise31(){
	x = 10;
	int s1 = f(x) + g(x);
	assert(s1 == 21);
	x = 10;
	int s2 = g(x) + f(x);
	assert(s2 == 19);
	int s3 = order(f(x), g(x));
	assert (s3 == 19);
}

int main(void) {
	exercise31(); /* prints !!!Hello World!!! */
	return EXIT_SUCCESS;
}
