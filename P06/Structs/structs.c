#include <stdio.h>
#include <math.h>

#define PI 3.14

typedef enum {
	LINE, CIRCLE, RECTANGLE, TRIANGLE
} ShapeKind;
typedef struct {
	double x, y;
} Point;
typedef struct {
	Point p1, p2;
} Line;
typedef struct {
	Point centre;
	int radius;
} Circle;
typedef struct {
	Point top_left;
	double width, height;
} Rectangle;
typedef struct {
	Point p1, p2, p3;
} Triangle;

typedef struct {   // Isto é um tipo SOMA, programado com a ajuda duma UNION
	ShapeKind kind;
	int color;
	union {
		Line line;
		Circle circle;
		Rectangle rectangle;
		Triangle triangle;
	} u;
} Shape;

Point point(double x, double y) // construtor de pontos
{
	Point p = { x, y };
	return p;
}

Shape line(Point p1, Point p2, int color) // construtor de linhas
{
	Line l = { p1, p2 };
	Shape s = { LINE, color };
	s.u.line = l;
	return s;
}

Shape triangle(Point p1, Point p2, Point p3, int color) {
	Shape s = { TRIANGLE, color};
	Triangle t = {p1, p2, p3};
	s.u.triangle = t;
	return s;
}

Shape circle(Point center, int radius, int color){
	Shape s = {CIRCLE, color};
	Circle c = {center, radius};
	s.u.circle = c;
	return s;
}

Shape rectangle(Point top_left, int width, int height, int color){
	Shape s = {RECTANGLE, color};
	Rectangle r = {top_left, width, height};
	s.u.rectangle = r;
	return s;
}

double area(Shape s){
	double v1 = 0, v2 = 0, v3 = 0, result = 0;
	switch(s.kind){
	case LINE:
		return 0;
	case CIRCLE:
		return (s.u.circle.radius << 2) * PI;
	case RECTANGLE:
		return s.u.rectangle.height * s.u.rectangle.width;
	case TRIANGLE:
		v1 = s.u.triangle.p1.x * (s.u.triangle.p2.y - s.u.triangle.p3.y);
		v2 = s.u.triangle.p2.x * (s.u.triangle.p3.y - s.u.triangle.p1.y);
		v3 = s.u.triangle.p3.x * (s.u.triangle.p1.y - s.u.triangle.p2.y);
		result = (v1 + v2 + v3) / 2;
		if (result >= 0) {
			return result;
		} else {
			return result * -1;
		}
	default:
		return 0;
	}
}

int main(void) {
	Shape c = circle(point(0, 0), 1, 99);
	printf("%lf\n", area(c));
	Shape t = triangle(point(0, 0), point(5, 10), point(10, 5), 99);
	printf("%lf\n", area(t));
	return 0;
}
