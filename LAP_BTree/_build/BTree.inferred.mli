type tree = Nil | Node of int * tree * tree
val make : int list -> tree
val max : tree -> int
val store : string -> tree -> unit
val load : string -> tree
val show_rec : tree -> string -> unit
val show : tree -> unit
