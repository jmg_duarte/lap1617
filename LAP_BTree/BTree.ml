type tree = Nil | Node of int * tree * tree ;;

let rec make l =
	match l with
	| [] -> Nil
	| x::xs -> Node(x, Nil, make xs)
;;

let rec max t =
	match t with
	| Nil -> failwith "max: Not defined for empty trees"
	| Node(x, Nil, Nil) -> x
	| Node(x, Nil, r) -> Pervasives.max x (max r)
	| Node(x, l, Nil) -> Pervasives.max x (max l)
	| Node(x, l, r) -> Pervasives.max x (Pervasives.max (max l) (max r))
;;

assert(try max Nil = 0 with Failure _ -> true);;

let store filename t =
	let co = open_out filename in
	let rec store_rec t =
		match t with
		| Nil -> output_string co "-\n"
		| Node(x, l, r) ->
				output_string co (string_of_int x ^ "\n");
				store_rec l;
				store_rec r
	in
	store_rec t;
	close_out co
;;

let load filename =
	let ci = open_in filename in
  let rec load_rec ci = 
		let s = input_line ci in
		if s = "-" then Nil
		else
  		let l = load_rec ci in
  		let r = load_rec ci in
  		Node(int_of_string s, l, r)
	in
	let t = load_rec ci in
	close_in ci ; t
;;

let rec show_rec t indent=
	match t with
	| Nil -> print_string "-\n"
	| Node(x,Nil,Nil) -> print_int x; print_string "\n"
	| Node(x,l,r) -> 
		print_int x;
		let indent' = indent ^ "\t" in
		show_rec l indent';
		show_rec r indent'
let show t = show_rec t ""
;;