type tree
val make : int list -> tree
val max : tree -> int
val store : string -> tree -> unit
val load : string -> tree
val show : tree -> unit
