#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define JUMP 5

typedef struct Node{
	int value;
	struct Node * next;
	struct Node * jump;
} Node, *List;

List newNode(int value, List next){
	List n = malloc(sizeof(Node));
	if(n==NULL) return NULL;
	n->value = value;
	n->next = next;
	n->jump = NULL;
}

int length (List l){
	int len = 0;
	if(l != NULL){
		len = 1;
		for(; l->jump != NULL; l = l->jump){
			len += JUMP;
		//	printf("%p\n", l->jump);
		}
		for(; l->next != NULL; l = l->next){
			len += 1;
		}
	}
	return len;
}

List last (List l){
	if(l != NULL){
		for(; l->jump != NULL; l = l->jump){
			
		}
		for(; l->next != NULL; l = l->next){
			
		}
	}
	return l;
}

List fillJump(List l){
	/*
	List start , old_node;
	start = old_node = l;
	for(int i = 0; l->next != NULL; l = l->next, i++){
		if(i - JUMP > 0){
			old_node->jump = l;
			old_node = old_node->next;
		}
	}
	return start;
	*/
	List start = l;
	for(; l->next != NULL; l = l->next){
		List lt = l;
		for (int i = 0; i < JUMP && lt->next != NULL; i++, lt = lt->next){
		}
		l->jump = lt;
	}
	return start;
}

void printJumps(List l){
	for(; l->next != NULL; l = l->next){
		printf("%p\n", l);		
	}
}

int main(int argc, char const *argv[])
{
	List l = newNode(0, newNode(1, newNode(2, newNode(3, newNode(4, NULL)))));
	int l1 = length(l);
	printf("%d\n", l1);
	printf("%d\n", last(l)->value);
	printf("%d\n", fillJump(l)->value);
	printJumps(l);
	int l2 = length(l);
	printf("%d\n", l2);
	assert(l1 == l2);
	free(l);
	return 0;
}