var JSRoot = {
    SUPER: function (method) {
        return method.apply(this,
            Array.prototype.slice.apply(arguments).slice(1));
    },
    INIT: function () {
        throw "*** MISSING INITIALIZER ***";
    }
};

function NEW(clazz) { // Create an object and applies INIT(...) to it
    function F() {
    }

    F.prototype = clazz;
    var obj = new F();
    obj.INIT.apply(obj, Array.prototype.slice.apply(arguments).slice(1));
    return obj;
}
function EXTENDS(clazz, added) { // Creates a subclass of a given class
    function F() {
    }

    F.prototype = clazz;
    var subclazz = new F();
    for (var prop in added)
        subclazz[prop] = added[prop];
    return subclazz;
}

var SkeletonElement = EXTENDS(JSRoot, {
    name : "",
    components: [],
    INIT: function(name){
        this.name = name;
    },
    basics : function(){
        return this.components.filter(function(component){
            return component instanceof BasicElement;
        });
    },
    weight : function(){
        var sumWeight = 0;
        for(var elem in components){
            if(components[elem] instanceof BasicElement){
                sumWeight += b[elem].weight;
            } else {
                sumWeight += b[elem].weight();
            }
        }
        return sumWeight;
    },
    validate : function(){
        var validity = true;
        for(var elem in components){
            if(components[elem] instanceof CompositeElement){
                if(!components[elem].validate()){
                    return validity;
                }
            }
        }
        return validity;
    }
});

var BasicElement = EXTENDS(SkeletonElement, {
    weight : 0,
    INIT: function(name, weight){
        this.SUPER(SkeletonElement.INIT, name);
        this.weight = weight;
    }
});

var CompositeElement = EXTENDS(SkeletonElement, {
    components : [],
    INIT: function(name, components){
        this.SUPER(SkeletonElement.INIT, name);
        this.components = components;
    }
    weight: function(){
        var w = 0;
        for(var elem in components){
            w += components[elem]
        }
    }
    validate: function(){
        if(components[elem] instanceof Joint){
            validity = components[elem].validate();
        } else if(components[elem] instanceof LogicPart){
            validity = components[elem].validate();
        }
    }
});

