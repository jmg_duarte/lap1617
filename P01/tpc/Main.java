import java.util.Scanner;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		System.out.println("I'm up\n");
		start();
	}

	private static int getchar() {
	    try {
	        return System.in.read() ;
	    } catch(IOException e) {
	        return -1 ;
	    }
	}

	public static void start(){
		switch(getchar()){
			case 'f': f(); break;
			case -1: fail(); break;
			default: start(); break;
		}
	}

	public static void f(){
		switch(getchar()){
			case 'o': f0(); break;
			case 'f': f(); break;
			case -1: fail(); break;
			default: start(); break;
		}
	}

	public static void f0(){
		switch(getchar()){
			case 'o': sucess(); break;
			case 'f': f(); break;
			case -1: fail(); break;
			default: start(); break;
		}
	}

	public static void fail(){
		System.out.println("REJECT.\n");
	}

	public static void sucess(){
		System.out.println("ACCEPT.\n");
	}
}